//
//  ViewController.swift
//  Lab3_iOS
//
//  Created by user131924 on 11/12/17.
//  Copyright © 2017 Camilla. All rights reserved.
//
import UIKit
import CoreLocation
import MapKit
import AVFoundation

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var theMap: MKMapView!
    @IBOutlet weak var theDistance: UILabel!
    
    @IBOutlet weak var theTime: UILabel!
    
    var manager: CLLocationManager!
    var myLocations: [CLLocation] = []
    
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var travelledDistance: Double = 0
    var counter: Double = 0

    var seconds = 0
    var timer = Timer()

    var applaud: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
        
        theMap.delegate = self
        theMap.mapType = MKMapType.standard
        theMap.showsUserLocation = true
        
        startTime()

    }
 
    func startTime() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ViewController.updateTime)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        seconds += 1
        theTime.text = timeString(time: TimeInterval(seconds))
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        myLocations.append(locations[0] as CLLocation)
    
        let spanX = 0.007
        let spanY = 0.007
        let newRegion = MKCoordinateRegion(center: theMap.userLocation.coordinate, span: MKCoordinateSpanMake(spanX, spanY))
    theMap.setRegion(newRegion, animated: true)
    
        if (myLocations.count > 1) {
            let sourceIndex = myLocations.count - 1
            let destinationIndex = myLocations.count - 2
        
            let c1 = myLocations[sourceIndex].coordinate
            let c2 = myLocations[destinationIndex].coordinate
            var a = [c1, c2]
            let polyline = MKPolyline(coordinates: &a, count: a.count)
            theMap.add(polyline)
        
            if startLocation == nil {
                startLocation = locations.last! as CLLocation
            
            } else{
                let lastLocation = locations.last as! CLLocation
                let distance = startLocation.distance(from: lastLocation)
                startLocation = lastLocation
                travelledDistance += distance

                theDistance.text = String(round(travelledDistance))
                
                playSound()
        }
    }
}
    func playSound(){
        let distance = round(travelledDistance) - counter
        if distance > 1000 {
            counter = round(travelledDistance)
            applaudSound()
        }
        else {
            print("keep on running")
        }
    }

    func applaudSound() {
        
        let path = Bundle.main.path(forResource: "99636__tomlija__small-crowd-yelling-yeah", ofType:"wav")!
        let url = URL(fileURLWithPath: path)
        
        do {
            applaud = try AVAudioPlayer(contentsOf: url)
            applaud?.play()
        } catch {
            // could not load file
        }
    }
    
func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    
    if overlay is MKPolyline {
        let poliLineRender = MKPolylineRenderer(overlay: overlay)
        poliLineRender.strokeColor = UIColor.green
        poliLineRender.lineWidth = 5
        return poliLineRender
    }
    return MKPolylineRenderer()
}

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    
    }

}
